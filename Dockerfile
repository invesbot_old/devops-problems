FROM node:10.13-alpine

WORKDIR /var/www/app

COPY . .

RUN npm install

EXPOSE 3000

CMD npm run start:dev