# Devops Test

## Prerequisites

You will need to install these tools to run the project.

1. Docker
2. docker-compose
3. Kubernetes
4. NodeJS (optional)

---

## Getting Started

Explore the repository and try to understand how the folders are structured (e.g. **migrations**, **routes**, **tests**).

Follow these steps to start the project.

1. Build all the images.

```sh
$ docker-compose build
```

2. Run all containers.

```sh
$ docker-compose up
```

3. Go to the `app` container and execute the migration.

```sh
# docker exec
$ docker-compose exec app /bin/sh

# run migration
$ npm run migrate:up
```

4. Go to [localhost:3000](http://localhost:3000) and start sending the request.

---

## Tasks

### 1. Create Dockerfile

You have to create a docker configuration file for the node js application that have multi-stage builds. There should be a stage to run the application's unit test. The configuration is aimed for production environment.

```sh
# run unit test
$ npm test
```

### 2. Create Kubernetes Deployment Files

You have to create deployment files for each services (i.e. application, database, and cache).
- Usually, each services have its own pod/deploy, configs, and service files.
- Application must have at least two replicas.
- Database must have a persistent volume.

### 3. Setup ELK Stack

You have to setup `elasticsearch`, `logstash`, `kibana`, and `metricbeat` for the application service. You may modify the application code to provide better logs for the monitoring service. If you do, please write in the documentation file as additional notes.

### 4. Create Documentation

You have to create a documentation on how to run all the services and any changes you made to the application code.

---

## Notes

Please, submit the solution to your own git repository and send the link to Aldrich Halim (aldrich.halim@pinnacleinvestment.co.id) and Marco Adrian (marco.adrian@pinnacleinvestment.co.id).

Good luck!