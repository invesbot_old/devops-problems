require('dotenv').config();

const express = require('express');
const path = require('path');
const logger = require('morgan');
const createError = require('http-errors');

const cacheConnector = require('./connectors/cache');
const dbConnector = require('./connectors/database');

const bookRouter = require('./routes/book');

const app = express();
app.locals.name = process.env.SERVICE_NAME || 'bookservice';

// initialization

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
} else if (process.env.NODE_ENV === 'production') {
  app.use(logger('combined'));
}

app.locals.redisClient = cacheConnector.initialize(app);
dbConnector.initialize();

// middleware pre-routes

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// routes

app.use('/books', bookRouter);

// middleware post-routes

app.use((req, res, next) => { // catch 404 and forward to error handler
  if (!res.headersSent) {
    return next(createError(404, `${req.method} ${req.originalUrl} not supported`));
  }
  return next();
});
app.use((err, req, res, next) => { // error handler
  if (!res.headersSent) {
    res.status(err.statusCode || 500);
    res.send(err.message);
  }
  return next();
});

module.exports = app;
