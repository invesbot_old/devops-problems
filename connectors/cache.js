/* eslint-disable no-console */

require('dotenv').config();
const Redis = require('ioredis');

exports.initialize = (app) => {
  if (!app.locals.redisClient) {
    const redisURL = process.env.NODE_ENV === 'development'
      ? 'cache' : process.env.REDIS_URL;

    const redisClient = new Redis(redisURL, {
      db: process.env.REDIS_DB,
      retryStrategy: () => 1000,
      maxRetriesPerRequest: 1,
      lazyConnect: true,
      password: process.env.REDIS_PASSWORD,
    });

    redisClient.connect()
      .then(() => {
        console.info(`[cache] successfully connected with status ${redisClient.status}!`);
      }, (err) => {
        console.error(`[cache] failed to connect with reason ${err}!`);
      })
      .catch((err) => {
        console.error(`[cache] failed to connect with reason ${err}!`);
      });

    redisClient.on('error', (err) => {
      console.error(`[cache] failed to connect with reason ${err}!`);
    });

    return redisClient;
  }
  return app.locals.redisClient;
};

exports.terminate = (app) => {
  if (app.locals.redisClient) {
    app.locals.redisClient.disconnect();
    console.info('[cache] successfully disconnected!');
  }
};
