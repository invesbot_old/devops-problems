/* eslint-disable no-console */

require('dotenv').config();
const mongoose = require('mongoose');

exports.initialize = () => {
  if (mongoose.connection.readyState === 0) {
    const mongoURL = process.env.NODE_ENV === 'development'
      ? `mongodb://db/${process.env.MONGO_DATABASE}` : `mongodb://${process.env.MONGODB_URL}`;

    mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      user: process.env.MONGO_USERNAME,
      pass: process.env.MONGO_PASSWORD,
    })
      .then(() => {
        console.info(`[db] successfully connected with status ${mongoose.STATES[mongoose.connection.readyState]}!`);
      }, (err) => {
        console.error(`[db] failed to connect to ${mongoURL}`);
        console.error(`[db] failed to connect with reason ${err}!`);
      });
  }
};

exports.terminate = () => {
  if (mongoose.connection.readyState === 1) {
    mongoose.disconnect()
      .then(() => {
        console.info('[db] successfully disconnected!');
      }, (err) => {
        console.error(`[db] failed to disconnect with reason ${err}!`);
      });
  }
};
