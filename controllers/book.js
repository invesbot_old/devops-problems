const createError = require('http-errors');

const Books = require('../models/book');

// map book list
const bookListPrefix = 'BookList';
exports.bookListPrefix = bookListPrefix;

// map book id to book detail
const bookDetailPrefix = 'Book_';
exports.bookDetailPrefix = bookDetailPrefix;

exports.getBookList = async (req, res, next) => {
  try {
    let bookList = await req.app.locals.redisClient.get(bookListPrefix);
    if (bookList !== null) {
      res.status(200);
      res.json(JSON.parse(bookList));
      return next();
    }

    const books = await Books
      .find()
      .sort({ createdAt: -1 })
      .lean()
      .exec();
    bookList = books.map((book) => ({
      // eslint-disable-next-line no-underscore-dangle
      id: book._id.toString(),
      title: book.title,
      author: book.author,
    }));

    await req.app.locals.redisClient.set(
      bookListPrefix,
      JSON.stringify(bookList),
      'EX',
      3600,
    );

    res.status(200);
    res.json(bookList);
    return next();
  } catch (err) {
    err.statusCode = 500;
    return next(err);
  }
};

exports.getBook = async (req, res, next) => {
  try {
    const { bookId } = req.params;

    let bookDetail = await req.app.locals.redisClient.get(bookDetailPrefix + bookId);
    if (bookDetail !== null) {
      res.status(200);
      res.json(JSON.parse(bookDetail));
      return next();
    }

    const book = await Books
      .findById(bookId)
      .lean()
      .exec();
    if (book === null) {
      return next(createError(400, 'Book not found'));
    }
    bookDetail = {
      // eslint-disable-next-line no-underscore-dangle
      id: book._id.toString(),
      title: book.title,
      author: book.author,
      isbn: book.isbn,
      numberOfPages: book.numberOfPages,
      country: book.country,
      publishedOn: book.publishedOn.toISOString(),
      updatedAt: book.updatedAt.toISOString(),
    };

    await req.app.locals.redisClient.set(
      bookDetailPrefix + bookId,
      JSON.stringify(bookDetail),
      'EX',
      3600,
    );

    res.status(200);
    res.json(bookDetail);
    return next();
  } catch (err) {
    err.statusCode = 500;
    return next(err);
  }
};

exports.addBook = async (req, res, next) => {
  try {
    const { title } = req.body;
    const { author } = req.body;
    const { isbn } = req.body;
    const { numberOfPages } = req.body;
    const { country } = req.body;
    const { publishedOn } = req.body;

    const book = await Books.create({
      title,
      author,
      isbn,
      numberOfPages,
      country,
      publishedOn,
    });

    let bookList = await req.app.locals.redisClient.get(bookListPrefix);
    if (bookList !== null) {
      bookList = JSON.parse(bookList);

      bookList.push({
        // eslint-disable-next-line no-underscore-dangle
        id: book._id,
        title: book.title,
        author: book.author,
      });

      await req.app.locals.redisClient.set(
        bookListPrefix,
        JSON.stringify(bookList),
        'EX',
        3600,
      );
    }

    return next();
  } catch (err) {
    err.statusCode = 500;
    return next(err);
  }
};

exports.updateBook = async (req, res, next) => {
  try {
    const { bookId } = req.params;
    const { title } = req.body;
    const { author } = req.body;
    const { isbn } = req.body;
    const { numberOfPages } = req.body;
    const { country } = req.body;
    const { publishedOn } = req.body;

    const book = await Books.findById(bookId).exec();
    if (book === null) {
      return next(createError(400, 'Book not found'));
    }

    book.title = title;
    book.author = author;
    book.isbn = isbn;
    book.numberOfPages = numberOfPages;
    book.country = country;
    book.publishedOn = publishedOn;
    await book.save();

    let bookList = await req.app.locals.redisClient.get(bookListPrefix);
    if (bookList !== null) {
      bookList = JSON.parse(bookList);

      const index = bookList.findIndex((b) => b.id === bookId);
      if (index > -1) {
        // eslint-disable-next-line no-underscore-dangle
        bookList[index].title = book.title;
        bookList[index].author = book.author;

        await req.app.locals.redisClient.set(
          bookListPrefix,
          JSON.stringify(bookList),
          'EX',
          3600,
        );
      }
    }

    let bookDetail = await req.app.locals.redisClient.get(bookDetailPrefix + bookId);
    if (bookDetail !== null) {
      bookDetail = JSON.parse(bookDetail);

      bookDetail.title = book.title;
      bookDetail.author = book.author;
      bookDetail.isbn = book.isbn;
      bookDetail.numberOfPages = book.numberOfPages;
      bookDetail.country = book.country;
      bookDetail.publishedOn = book.publishedOn;
      bookDetail.updatedAt = book.updatedAt;

      await req.app.locals.redisClient.set(
        bookDetailPrefix + bookId,
        JSON.stringify(bookDetail),
        'EX',
        3600,
      );
    }

    return next();
  } catch (err) {
    err.statusCode = 500;
    return next(err);
  }
};

exports.removeBook = async (req, res, next) => {
  try {
    const { bookId } = req.params;

    const book = await Books.findByIdAndDelete(bookId).exec();
    if (book === null) {
      return next(createError(400, 'Book not found'));
    }

    let bookList = await req.app.locals.redisClient.get(bookListPrefix);
    if (bookList !== null) {
      bookList = JSON.parse(bookList);

      const index = bookList.findIndex((b) => b.id === bookId);
      if (index > -1) {
        bookList.splice(index, 1);

        await req.app.locals.redisClient.set(
          bookListPrefix,
          JSON.stringify(bookList),
          'EX',
          3600,
        );
      }
    }

    const bookDetail = await req.app.locals.redisClient.get(bookDetailPrefix + bookId);
    if (bookDetail !== null) {
      await req.app.locals.redisClient.del(bookDetailPrefix + bookId);
    }

    return next();
  } catch (err) {
    err.statusCode = 500;
    return next(err);
  }
};
