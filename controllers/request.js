const Joi = require('@hapi/joi');

exports.validate = (schema) => (req, res, next) => {
  try {
    Object.keys(schema).forEach((key) => {
      Joi.assert(req[key], schema[key], (err) => {
        if (err) {
          throw err;
        }
      });
    });
    next();
  } catch (err) {
    res.status(400);
    res.json({ message: err.message });
    next(err);
  }
};
