exports.successResponse = (msg) => (req, res, next) => {
  res.status(200);
  res.send(msg);
  return next();
};
