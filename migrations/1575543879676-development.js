const db = require('../connectors/database');
const cache = require('../connectors/cache');
const book = require('../models/book');
const seed = require('../seeds/book');

const appLocals = {
  locals: {},
};

module.exports.up = async () => {
  try {
    db.initialize();
    const cacheClient = cache.initialize(appLocals);

    // populate the db
    await book.insertMany(seed);

    // clear the cache
    await cacheClient.flushall('ASYNC');
  } catch (error) {
    console.error('[db] migration failed: ', error);
  } finally {
    db.terminate();
    cache.terminate(appLocals);
  }
};

module.exports.down = async () => {
  try {
    db.initialize();
    const cacheClient = cache.initialize(appLocals);

    await book.deleteMany({});

    // clear the cache
    await cacheClient.flushall('ASYNC');
  } catch (error) {
    console.error('[db] migration failed: ', error);
  } finally {
    db.terminate();
    cache.terminate(appLocals);
  }
};
