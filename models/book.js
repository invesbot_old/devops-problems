const mongoose = require('mongoose');

const { Schema } = mongoose;

const bookSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: false,
    index: true,
  },
  author: {
    type: String,
    required: true,
    unique: false,
    index: true,
  },
  isbn: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  numberOfPages: {
    type: Number,
    required: true,
    unique: false,
    index: false,
  },
  country: {
    type: String,
    required: true,
    unique: false,
    index: false,
  },
  publishedOn: {
    type: Date,
    required: true,
    unique: false,
    index: false,
  },
}, {
  timestamps: true,
});

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
