const bodyParser = require('body-parser');
const express = require('express');

const bookController = require('../controllers/book');
const requestController = require('../controllers/request');
const responseController = require('../controllers/response');

const bookValidator = require('../validators/book');

const router = express.Router();
router.use(bodyParser.json());

router.get('/',
  bookController.getBookList);

router.get('/:bookId',
  requestController.validate(bookValidator.getBook),
  bookController.getBook);

router.post('/',
  requestController.validate(bookValidator.addBook),
  bookController.addBook,
  responseController.successResponse('Book added successfully'));

router.put('/:bookId',
  requestController.validate(bookValidator.updateBook),
  bookController.updateBook,
  responseController.successResponse('Book updated successfully'));

router.delete('/:bookId',
  requestController.validate(bookValidator.removeBook),
  bookController.removeBook,
  responseController.successResponse('Book removed successfully'));

module.exports = router;
