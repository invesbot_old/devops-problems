/* eslint-disable no-unused-expressions */

const chai = require('chai');
const mongoose = require('mongoose');
const rewire = require('rewire');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
require('sinon-mongoose');

const bookController = rewire('../controllers/book');

const bookModel = require('../models/book');

chai.use(sinonChai);

const { expect } = chai;

describe('[book] getBook()', () => {
  const now = new Date();

  const bookDetailPrefix = 'Book_';
  const bookId = mongoose.Types.ObjectId();
  const bookDocument = {
    _id: bookId,
    title: 'Customer Grocery bypassing',
    author: 'Ayla Bechtelar',
    isbn: '1705ab0d-e64a-4d2b-96e3-40f79ccccd69',
    publishedOn: now,
    numberOfPages: 16,
    country: 'Pitcairn Islands',
    updatedAt: now,
    createdAt: now,
  };

  const app = {
    locals: {
      name: process.env.SERVICE_NAME || 'bookservice',
    },
  };

  let req;
  let res;
  let next;

  beforeEach((done) => {
    app.locals.redisClient = {
      get: sinon.stub(),
      set: sinon.spy(),
    };
    req = {
      app,
      params: { bookId: bookId.toString() },
    };
    res = {
      json: sinon.spy(),
      status: sinon.spy(),
    };
    next = sinon.spy();
    done();
  });

  beforeEach((done) => {
    this.mockBookModel = sinon.mock(bookModel);
    done();
  });

  afterEach((done) => {
    this.mockBookModel.restore();
    done();
  });

  it('should return 200 and book detail from database', (done) => {
    req.app.locals.redisClient.get.resolves(null);

    const findBook = this.mockBookModel.expects('findById');
    findBook
      .once()
      .withArgs(req.params.bookId)
      .chain('lean')
      .chain('exec')
      .resolves(bookDocument);

    bookController.getBook(req, res, next)
      .then(() => {
        const { redisClient } = req.app.locals;
        expect(redisClient.get).to.have.been.calledOnceWith(bookDetailPrefix + req.params.bookId);

        findBook.verify();
        expect(findBook).to.have.been.calledAfter(redisClient.get);
        expect(findBook).to.have.been.calledBefore(redisClient.set);

        expect(redisClient.set).to.have.been.calledOnce;

        const [key, payload] = redisClient.set.getCall(0).args;
        const bookDetail = JSON.parse(payload);

        expect(key).to.eql(bookDetailPrefix + req.params.bookId);
        // eslint-disable-next-line no-underscore-dangle
        expect(bookDetail).to.have.property('id', bookDocument._id.toString());
        expect(bookDetail).to.have.property('title', bookDocument.title);
        expect(bookDetail).to.have.property('author', bookDocument.author);
        expect(bookDetail).to.have.property('isbn', bookDocument.isbn);
        expect(bookDetail).to.have.property('numberOfPages', bookDocument.numberOfPages);
        expect(bookDetail).to.have.property('country', bookDocument.country);
        expect(bookDetail).to.have.property('publishedOn', bookDocument.publishedOn.toISOString());
        expect(bookDetail).to.have.property('updatedAt', bookDocument.updatedAt.toISOString());

        expect(res.status).to.have.been.calledOnceWith(200);
        expect(res.status).to.have.been.calledAfter(redisClient.set);
        expect(res.json).to.have.been.calledOnceWith(bookDetail);
        expect(res.json).to.have.been.calledAfter(redisClient.set);

        expect(next).to.have.been.calledOnceWith();
        expect(next).to.have.been.calledAfter(res.status);
        expect(next).to.have.been.calledAfter(res.json);
        done();
      })
      .catch((err) => done(err));
  });

  it('should return 400 if book is not found', (done) => {
    req.app.locals.redisClient.get.resolves(null);

    const findBook = this.mockBookModel.expects('findById');
    findBook
      .once()
      .withArgs(req.params.bookId)
      .chain('lean')
      .chain('exec')
      .resolves(null);

    const createError = sinon.stub().returns('TestError');
    // eslint-disable-next-line no-underscore-dangle
    const revertError = bookController.__set__('createError', createError);

    bookController.getBook(req, res, next)
      .then(() => {
        findBook.verify();
        expect(createError).to.have.been.calledOnceWith(400, 'Book not found');
        expect(createError).to.have.been.calledAfter(findBook);
        expect(next).to.have.been.calledOnceWith('TestError');
        done();
      })
      .catch((err) => done(err))
      .finally(() => revertError());
  });

  it('should return 500 if an error is thrown', (done) => {
    req.app.locals.redisClient.get.throws('TestError');
    bookController.getBook(req, res, next)
      .then(() => {
        expect(next).to.have.been.calledOnce;
        const [error] = next.getCall(0).args;
        expect(error).to.have.property('statusCode', 500);
        expect(error).to.have.property('name', 'TestError');
        done();
      })
      .catch((err) => done(err));
  });
});
