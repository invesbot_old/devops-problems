/* eslint-disable no-unused-expressions */

const chai = require('chai');
const mongoose = require('mongoose');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
require('sinon-mongoose');

const bookController = require('../controllers/book');

const bookModel = require('../models/book');

const bookSeeds = require('../seeds/book');

chai.use(sinonChai);
const { expect } = chai;

describe('[book] getBookList()', () => {
  const bookListPrefix = 'BookList';
  const bookDocuments = bookSeeds.map((bookSeed) => ({
    _id: mongoose.Types.ObjectId(),
    ...bookSeed,
  }));

  const app = {
    locals: {
      name: process.env.SERVICE_NAME || 'bookservice',
    },
  };

  let req;
  let res;
  let next;

  beforeEach((done) => {
    app.locals.redisClient = {
      get: sinon.stub(),
      set: sinon.spy(),
    };
    req = { app };
    res = {
      json: sinon.spy(),
      status: sinon.spy(),
    };
    next = sinon.spy();
    done();
  });

  beforeEach((done) => {
    this.mockBookModel = sinon.mock(bookModel);
    done();
  });

  afterEach((done) => {
    this.mockBookModel.restore();
    done();
  });

  it('should return 200 and booklist from database', (done) => {
    req.app.locals.redisClient.get.resolves(null);

    const findBooks = this.mockBookModel.expects('find');
    findBooks
      .once()
      .chain('sort')
      .withArgs({ createdAt: -1 })
      .chain('lean')
      .chain('exec')
      .resolves(bookDocuments);

    bookController.getBookList(req, res, next)
      .then(() => {
        const { redisClient } = req.app.locals;
        expect(redisClient.get).to.have.been.calledOnceWith(bookListPrefix);

        findBooks.verify();
        expect(findBooks).to.have.been.calledAfter(redisClient.get);
        expect(findBooks).to.have.been.calledBefore(redisClient.set);

        expect(redisClient.set).to.have.been.calledOnce;

        const [key, payload] = redisClient.set.getCall(0).args;
        const bookList = JSON.parse(payload);

        expect(key).to.eql(bookListPrefix);
        expect(bookList).to.be.an('array').that.has.length(bookDocuments.length);
        bookList.forEach((book, index) => {
          // eslint-disable-next-line no-underscore-dangle
          expect(book).to.have.property('id', bookDocuments[index]._id.toString());
          expect(book).to.have.property('title', bookDocuments[index].title);
          expect(book).to.have.property('author', bookDocuments[index].author);
        });

        expect(res.status).to.have.been.calledOnceWith(200);
        expect(res.status).to.have.been.calledAfter(redisClient.set);
        expect(res.json).to.have.been.calledOnceWith(bookList);
        expect(res.json).to.have.been.calledAfter(redisClient.set);

        expect(next).to.have.been.calledOnceWith();
        expect(next).to.have.been.calledAfter(res.status);
        expect(next).to.have.been.calledAfter(res.json);
        done();
      })
      .catch((err) => done(err));
  });

  it('should return 500 if an error is thrown', (done) => {
    req.app.locals.redisClient.get.throws('TestError');
    bookController.getBookList(req, res, next)
      .then(() => {
        expect(next).to.have.been.calledOnce;
        const [error] = next.getCall(0).args;
        expect(error).to.have.property('statusCode', 500);
        expect(error).to.have.property('name', 'TestError');
        done();
      })
      .catch((err) => done(err));
  });
});
