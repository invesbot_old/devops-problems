const Joi = require('@hapi/joi');

exports.getBook = {
  params: Joi.object().keys({
    bookId: Joi.string()
      .regex(/^[A-Fa-f0-9]{24,24}$/)
      .required()
      .error(new Error('Invalid bookId format')),
  }),
};

exports.addBook = {
  body: Joi.object().keys({
    title: Joi.string()
      .regex(/^\S[\S ]{1,256}\S$/)
      .required()
      .error(new Error('Invalid title format')),
    author: Joi.string()
      .regex(/^\S[\S ]{1,128}\S$/)
      .required()
      .error(new Error('Invalid author format')),
    isbn: Joi.string().uuid()
      .required()
      .error(new Error('Invalid isbn format')),
    numberOfPages: Joi.number()
      .required()
      .error(new Error('Invalid numberOfPages format')),
    country: Joi.string()
      .required()
      .error(new Error('Invalid country format')),
    publishedOn: Joi.date()
      .required()
      .error(new Error('Invalid publishedOn format')),
  }),
};

exports.updateBook = {
  params: Joi.object().keys({
    bookId: Joi.string()
      .regex(/^[A-Fa-f0-9]{24,24}$/)
      .required()
      .error(new Error('Invalid bookId format')),
  }),
  body: Joi.object().keys({
    title: Joi.string()
      .regex(/^\S[\S ]{1,256}\S$/)
      .required()
      .error(new Error('Invalid title format')),
    author: Joi.string()
      .regex(/^\S[\S ]{1,128}\S$/)
      .required()
      .error(new Error('Invalid author format')),
    isbn: Joi.string().uuid()
      .required()
      .error(new Error('Invalid isbn format')),
    numberOfPages: Joi.number()
      .required()
      .error(new Error('Invalid numberOfPages format')),
    country: Joi.string()
      .required()
      .error(new Error('Invalid country format')),
    publishedOn: Joi.date()
      .required()
      .error(new Error('Invalid publishedOn format')),
  }),
};

exports.removeBook = {
  params: Joi.object().keys({
    bookId: Joi.string()
      .regex(/^[A-Fa-f0-9]{24,24}$/)
      .required()
      .error(new Error('Invalid bookId format')),
  }),
};
